import time

import redis
from flask import Flask, request

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

#coucou commentaire test

@app.route("/", methods=['POST'])
def record():
	request.get_data()
	timestamp = time.time()
	data = request.data
	cache.set(timestamp,data)
	return data

@app.route("/", methods=['GET'])
def getdata():
	return (str(cache.keys()))

if __name__ == "__main__":
    app.run(host='0.0.0.0')
